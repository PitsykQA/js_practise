function sayHi() {
    var phrase = "Hello"; // local variable, "var" instead of "let"

    console.log(phrase); // Hello
}
sayHi();

//console.log(phrase); // Cannot get variable because of function scope .

// “var” has no block scope. var variables are either function-wide or global, they are visible through blocks.


if(true){
    var test = 'Let will not be visible here';
    let testTwo = 'Yes, var is absolutely true';
}

console.log(test);
//console.log(testTwo); // Error -> testTwo is not defined.

'use strict';

const firstLetterToUpperCase = str => console.log(str[0].toUpperCase() + str.slice(1));
firstLetterToUpperCase('andrii');
// Also we can user str.charAt(0);


function checkSpame(str){
    const lowerCase = str.toLowerCase();
    return lowerCase.includes('viagra') ||lowerCase.includes('xxxxx');
}

console.log(checkSpame('buy ViAgRA now'));
console.log(checkSpame('free xxxxx'));
console.log(checkSpame("innocent rabbit"));


const trimSpaces = ' I don\' like space ';

console.log(trimSpaces.trim());

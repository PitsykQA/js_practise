'use strict';
// Solution FIRST
let salaries = {
    "John": 100,
    "Pete": 300,
    "Adam": 250
};

function topSalare(salaries) {
    let result = 0;
    for (let [name, value] of Object.entries(salaries)) {
        if (result < value) {
            result = value;
        }
    }
        return Object.keys(salaries).find(key => salaries[key] === result);
}
console.log(topSalare(salaries));

// Solution SECOND


function topSalaryTwo(salaries) {
    let result = 0;
    let maxName = null;

    for (let [name, salary] of Object.entries(salaries)){
        if (result <salary){
            result = salary;
            maxName = name;
        }
    }
    return maxName;
}

console.log('Solution two', topSalaryTwo(salaries));


// E.X 2
let user = { name: "John", years: 30 };
let {name, years, isAdmin = false} = user;
console.log(name, years, isAdmin);

'use strict';

let options = {
    'title': 'Menu',
    'width': 100,
    'height': 200
}

let {title, width, height} = options;
console.log(title, width, height);

// NOTE-> The order of variables in {} doesn't matter. {width, height, title} will have the same assigned variable
let{firstName, lastName, year} = {year: 1991, firstName: 'Andrii', lastName: 'Pitsyk'};
console.log(firstName,lastName,year); // Andrii Pitsyk 1991

// NOTE -> If we want change variable name then we should do following:

let optionsTwo = {
    'title': 'Menu',
    'width': 100,
    'height': 200
};

let {width: w, height: h, title: t} = optionsTwo; // The existing object isn't changed.
console.log(w, h, t); // 100 200 'Menu'

// DEFAULT VALUES.

let employer = {
    position: 'QA'
};
// DEFAULT VALUES CAN A FUNCTION CALL;
let {empMame = 'Andrii', empLastName = 'Pitsyk', position} = employer;

console.log(employer); // doesn't changed
console.log(empMame, empLastName,position); // changed;

//REST OPERATOR

let vegetables = {
    apple: '2$',
    banana: '4$',
    orange: '1$'
}

let {apple, ...allOther} = vegetables;
console.log(apple, allOther);

//NOTE -> IF WE DON'T KNOW HOW MANY PROPERTIES HAS OBJECT WE CAN SAVE THIS VALUE WITCH WE NEED TO VARIABLE AND ALL OTHER
// SAVE TO ONE VARIABLE USING REST OPERATOR;

// DO NOT USE THIS STYLE ->>>> ERROR
let mikle, rob, dirk;

// error in this line. JS engine think that it's code block not Destructing assigment;
//{title, width, height} = {title: "Menu", width: 200, height: 100};
// Wraping in () will fix the problem;

({mikle, rob, dirk} = {mikle: "Menu", rob: 200, dirk: 100});
console.log(mikle, rob, dirk);

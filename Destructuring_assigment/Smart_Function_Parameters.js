'use strict';

// BAD WAY TO WRITE A FUNCTION
function showMenuBadStyle(title = "Untitled", width = 200, height = 100, items = []) {
    //There are several problems:
    //1. The problem is how to remember the order of arguments
    //2. The problem is how to call a function when most parameters are ok by default.
    //showMenu("My Menu", undefined, undefined, ["Item1", "Item2"]) -> That’s ugly. And becomes unreadable when we deal with more parameters.
}

// Destructuring comes to the rescue!

let options = {
    title: "My menu",
    items: ["Item1", "Item2"]
};
function showMenu({title = 'Untiled', width = 200, height = 100, items = []}) {
    console.log(`${title} ${width} ${height}`);
    console.log(items);
}
showMenu(options); // Please note that such destructuring assumes that showMenu() does have an argument

showMenu({});// All values by default

//showMenu() // Error. To fix this we should specify default value {} for value for the whole destructuring thing

function showMenuFix({title = 'Untiled', width = 200, height = 100, items = []} = {}) {
    console.log(`${title} ${width} ${height}`);
    console.log(items);
};
showMenuFix();  // This is OK


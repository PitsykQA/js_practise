'use strict';
const user = ['Andrii', 'Pitsyk'];
// Destructuring assigment
let [firstName, lastName] = user;
let [name, sureName] = 'Andrii Pitsyk'.split(' ');
console.log(firstName, lastName);
console.log(name, sureName);

//let firstName = arr[0]; let surname = arr[1]; Destructuring assigment is much  better

// Unwanted elements of the array can also be thrown away via an extra comma:

let [first, , title] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];
console.log(first); // Julius
console.log( title ); // Consul. Caesar is skipped;

//Works with any iterable on the right-side
let[a, b, c] = 'abc';
console.log(a, b, c);

let [one, two, three] = new Set([1, 2, 3]);
console.log(one, two, three);

//Assign to anything at the left-side
let userAs = {};
[userAs['name'], userAs.surname] = 'Ilya Kantor'.split(' ');
console.log(userAs);


// Looping with Plain Object (Object.entries) & Map

let userIterator = {name: 'Andrii', age: 120 };

for(let [key, value] of Object.entries(userIterator)){
    console.log(`${key}:${value}`);
}

let mapIteration = new Map();
mapIteration.set(1,'test').set(2, 'test');

for(let [key, value] of mapIteration.entries()){
    console.log(`${key}:${value}`);
}

// REST OPERATOR IN DESTRUCTING ASSIGMENT
                   // we can use any value instead of rest;
let [name1, name2, ...rest] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];
console.log(name1, name2, rest); // rest will be [ 'Consul', 'of the Roman Republic' ]


// DEFAULT VALUE

let[userName = 'Guest', userSureName = 'Anonymous'] = ['Julius'];
console.log(userName, userSureName);

// Default value cam be event function.













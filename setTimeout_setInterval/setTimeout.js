'use strict';

function sayHi() {
    console.log('Hello');
}

setTimeout(sayHi, 2000);


function sayHiTwo(name) {
    console.log(`Hi ${name}`);
}

setTimeout(sayHiTwo, 2000,'Andrii');

//setTimeout(sayHi(), 1000); -> doesn’t work, because setTimeout expects a reference to function.
// And here sayHi() runs the function and the result of its execution is passed to setTimeout. In our case the result of sayHi() is undefined (the function returns nothing), so nothing is scheduled.

// Canceling with clearTimeout

let cancelTime = setTimeout(function () {
    console.log('Cancel timeout')
}, 5000);

clearTimeout(cancelTime);


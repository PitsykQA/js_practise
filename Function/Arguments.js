// There is also a special array-like object named arguments that contains all arguments by their index.
function showName() {
    console.log( arguments.length );
    console.log( arguments[0] );
    console.log( arguments[1] );

    // it's iterable
    // for(let arg of arguments) console.log(arg);
}

// shows: 2, Julius, Caesar
showName("Julius", "Caesar");

// shows: 1, Ilya, undefined (no second argument)
showName("Ilya");

//But the downside is that although arguments is both array-like and iterable, it’s not an array.
// It does not support array methods, so we can’t call arguments.map(...) for example


// ARROW FUNCTION DOESN'T HAVE ARGUMENTS
let testArrow = () =>{
    console.log('The result is' , arguments[0]);
};
testArrow(1);

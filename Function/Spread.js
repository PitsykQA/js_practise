function sum(a, b) {
    return a + b;
}

console.log( sum(1, 2, 3, 4, 5) ); // No error but only first 2 values will be counted

// REST - means gather all information in array

function sumAll(...args) {
    const  test = args.reduce((previous, current) => {
        return previous + current;
    });
    return test;
    }
console.log(sumAll(1,2,3,4,5));


function showUserInfo(firstName, lastName, ...allOther) {
    console.log(`${firstName} ${lastName}`);
    console.log(allOther);
}

showUserInfo("Julius", "Caesar", "Consul", "Imperator");

//The ...rest must always be last argument.
// function f(arg1, ...rest, arg2) { // arg2 after ...rest ?!
//     // error
// }



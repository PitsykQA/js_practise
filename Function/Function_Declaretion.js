'use strict';
const userName = 'Andrii';

function showMessage(){
    let userName = 'Bob';
    let message = 'Hello, I\'m Test Automation Engnier ' + userName;
    console.log(message);
}

//showMessage();
//console.log(userName);

// The variable outside woun't be over written. In function they will use local variable.

const text = 'Andrii';

function showMessage(from , text = 'Default text') {
    console.log(`${from} ${text}`);
}
// Call function with declarated variable;
showMessage('Hello', text);
// Call function without text argument to apply default value;
showMessage('Show me');


function tellName(fullName = generateFullName()) {
    console.log(fullName);
}

function generateFullName() {
    return 'Andrii Pitsyk';
}

tellName('Igor Vavelon');
// Here tell me function will call anothre function which form this value;
tellName();

// Check another example
const age = 18;
function checkAge(age){
    if (age >= 18 ){
        return true;
    } else {
        return false;
    }
}
if (checkAge(age)){
    console.log('Ok, I will sell your a bear');
}else{
    console.log('Noooooo');
}


// Nothing return from function;
function nothingReturn() {
    console.log('Nothing return');
}

console.log(nothingReturn()===undefined);


function checkAge(age) {
    return age > 18 ? console.log(`I'm ${age}`) : console.log('You are not permit to perform this action');
}

checkAge(17);

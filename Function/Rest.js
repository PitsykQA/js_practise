// REST - Makes list of variables from array

//When ...arr is used in the function call, it “expands” an iterable object arr into the list of arguments.

let arr = [1,2,3,10];

console.log(Math.max(...arr)); // 10 (spread turns array into a list of arguments)


let arr1 = [2,3,1];
let arr2 = [4,9,12];

console.log(Math.max(...arr1, ...arr2)); // 12
console.log(Math.max(...arr1,5, 7, ...arr2, 32)); // 32 combine with normal value

// Merge two arrays

let merge1 = ['Andrii', 'Pitsyk'];
let merge2 = ['1991', 'AQA'];

let userInfo = [...merge1, ...merge2];
console.log(userInfo);


'use strict';

const showName = function () {
    return 'Andrii Pitsyk';
}

console.log(showName());

//Copy function declaration function
function showData() {
    return new Date();
}

const date = showData;
console.log(date());



//Call back
function first(number, callback){
    setTimeout(function (){
        console.log(number)
        callback();
    },3000);
}

function second() {
    return console.log('2')}

first('33', second );


function doHomeWork(subject, callback){
    console.log(`Starting my ${subject}`);
    callback();
}

doHomeWork('math', function(){
    console.log('Finished my work');
});


//DIFFERENCE BETWEEN FUNCTION DECLARATION AND FUNCTION EXPRESSION;
//when JavaScript prepares to run the script or a code block, it first looks for Function Declarations in it and creates the functions.
// We can think of it as an “initialization stage”.
callFunctionDeclarationBeforeBody();
function callFunctionDeclarationBeforeBody() {
    return console.log('This message will be shown 100%');
}

// The reason is why this function is't executed is that the JS engine create function not on compiling state but
// on state when code is executing and function is assigned to the variable.

//callFunctionExpressionBeforeBody(); -> ERROR
const callFunctionExpressionBeforeBody = function () {
    return console.log('This function won\'t be executed');
};


// VISIBILITY OF FUNCTION
const checkAge = 12;
if (checkAge > 18){
    allowCar();
    function allowCar() {
         console.log('I can drive a car!!!!!!');
    }
}else{
    allowCar();

    function allowCar() {
        console.log('I suddenly can not drive a car =((((');
    }
}

// allowCar(); -> Can't call the function because it is visible only in if block

const verifyAge = 21;
let giveAccess;

if (verifyAge > 18){
    giveAccess = function() {
        console.log('I can drive a car!!!!!!');
    }
}else{
    giveAccess = function() {
        console.log('I suddenly can not drive a car =((((');
    }
}
giveAccess(); // -> Now I can because we assign function to the variable which is out of if block

// ARROW FUNCTION

const sum = (a,b)=> a+b;
console.log(sum(1,2));

// For one argument we don't we can be omit parentheses
const double = n => n*2;
console.log(double(3));

// No arguments
const sayHello = () => console.log('Hello');
sayHello();


// With more complex expressions
let sumSecond = (a, b)=>{
    const result = a+b;
    return result;
}
console.log(sumSecond(1,2));

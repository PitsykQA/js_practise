function SuperType(firstName, lastName){
        this.firstName = firstName,
        this.lastName = lastName,
        this.friends = ["Ashwin", "Jadeja"]
}

//SuperType prototype
SuperType.prototype.getSuperName = function(){
    return this.firstName + " " + this.lastName;
}

//SubType prototype function
function SubType(firstName, lastName, age){
    //Inherit instance properties
    SuperType.call(this, firstName, lastName); // SubType constructor function then calls SuperType constructor function using call SuperType.call(this, firstname, lastName) this here represent the subTypeObj1
    // After the return of SuperType.call(this, firstname, lastName), SubType constructor function adds a age property to subTypeObj1 object.
    this.age = age;
}

//Inherit methods and shared properties
SubType.prototype = new SuperType(); // Here we inherit all methods which are in Supper type prototype but not the Suppert type values.

//Add new property to SubType prototype
SubType.prototype.getSubAge = function(){
    return this.age;
}

//Create SubType objects
var subTypeObj1= new SubType("Virat", "Kohli", 26);
console.log(subTypeObj1.getSuperName());
console.log(subTypeObj1.age);
console.log(subTypeObj1.friends);
console.log(subTypeObj1.getSubAge());


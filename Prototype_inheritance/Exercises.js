let head = {
    glasses: 1
};

let table = {
    pen: 3,
    __proto__: head
};

let bed = {
    sheet: 1,
    pillow: 2,
    __proto__: table
};

let pockets = {
    money: 2000,
    __proto__: bed
};


console.log(pockets.pen);
console.log(bed.glasses);


// Exercises 2

// let hamster = {
//     stomach: [],
//     eat(food) {
//         this.stomach.push(food);
//     }
// };
//
// let speedy = {
//     __proto__: hamster
// };
//
// let lazy = {
//     __proto__: hamster
// };
//
// // This one found the food
// speedy.eat("apple");
// lazy.eat('orange');
// console.log( speedy.stomach ); // apple
//
// // This one also has it, why? fix please.
// console.log( lazy.stomach ); // apple


// SOLUTION
// https://javascript.info/prototype-inheritance#writing-doesn-t-use-prototype
// Every time the stomach is taken from the prototype, then stomach.push modifies it “at place”.
// Please note that such thing doesn’t happen in case of a simple assignment this.stomach=[food];

let hamster = {
    stomach: [],
    eat(food){
        this.stomach = [food];
    }
};

let speedy = {
    __proto__:hamster,
};

let lazy = {
    __proto__: hamster,
};

speedy.eat('apple');
console.log(speedy.stomach);
console.log(lazy.stomach);



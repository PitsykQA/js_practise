//The for..in loops over inherited properties too.

let animal = {
    eats: true,
    sleep: true,
    breath: true
};

let rabbit = {
    jumps: true,
    __proto__:animal
};

console.log(Object.keys(rabbit)); // only jumps

for(let key in rabbit){
    console.log(key); // Show all properties of rabbit and inherited object;
};


// Filtering owen and inherited properties

let animalTwo = {
    eats: true,
    sleep: true,
    breath: true
};

let rabbitTwo = {
    jumps: true,
    __proto__:animalTwo
};

for(let key in rabbit){
    let isOwnProprety = rabbit.hasOwnProperty(key);
    isOwnProprety ? console.log(`Own property -> ${key}`) : console.log(`Inherited property ${key}`);
};

// Two object inherit of animal
let animals = {
    eats: true,
    walk(){
        return console.log('Animal walk');
    },
    sleep(){
       return console.log('Animal sleep');
    }
};

let rabbit = {
    jumps: true
};

let hourse = {
    run: true
};

rabbit.__proto__ = animals;
hourse.__proto__ = animals;

console.dir(rabbit);
console.dir(hourse);

console.log(rabbit.eats);
console.log(rabbit.walk());
console.log(rabbit.sleep());


console.dir(hourse.eats);
console.log(hourse.walk());
console.log(hourse.sleep());

// Prototype chain example

let animalTwo = {
    eats: true,
    walks(){
        return console.log('Animal walk');
    }
};

let rabbitTwo = {
    jumps: true,
    __proto__: animalTwo
};

let longEar = {
    earLength: 10,
    __proto__:rabbit
};

longEar.walk();



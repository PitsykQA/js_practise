
let user = {
    eat: true,
    walk() {
        console.log('Walk true');
    }
};

let andrii = {
    __proto__: user
};
andrii['walk'] = function(){
     console.log('Andrii can walk');
};

console.log(andrii);
console.log(andrii.walk());


// Example two
let userTwo = {
    name: "John",
    surname: "Smith",

    set fullName(value) {
        [this.name, this.surname] = value.split(" ");
    },

    get fullName() {
        return `${this.name} ${this.surname}`;
    }
};

let admin = {
    __proto__: userTwo,
    isAdmin: true
};

console.log(admin.fullName); // John Smith (*)

// setter triggers!
admin.fullName = "Alice Cooper"; // (**) Here we don't assign property for object. We call setter. To call setter we don't need -> ();



// Example three

let animal = {
    walk() {
        if (!this.isSleeping) {
            alert(`I walk`);
        }
    },
    sleep() {
        this.isSleeping = true;
    }
};

let rabbit = {
    name: "White Rabbit",
    __proto__: animal
};

// modifies rabbit.isSleeping. Set rebbit isSleeping as true;
rabbit.sleep();

console.log(rabbit.isSleeping); // true
console.log(animal.isSleeping); // undefined. Animal doesn't have this property. When inherited object execute method
// from his parent, parent is not affected.


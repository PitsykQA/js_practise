'use strict';
function SuperType() {
    this.name = 'Andrii';
};

SuperType.prototype.getSupperName = function(){
    return this.name;
};

function SubType(){
    this.age = 26;
};

SubType.prototype = new SuperType();

SubType.prototype.getSubAge = function () {
    return this.age;
};

console.log('');
let subTypeObject = new SubType();
console.log(subTypeObject.name);
console.log(subTypeObject.age);
console.log(subTypeObject.getSupperName());
console.log(subTypeObject.getSubAge());



'use strict';
function AnimalHome() {
};

AnimalHome.prototype.homeAnimal = [];

AnimalHome.prototype.createAnimalHome = function() {
    if (this.homeAnimal.length == 0) {
        this.homeAnimal.push({type: this.animalType, home: []})
    } else {
        this.homeAnimal.forEach((value) => {
           return value.type !== this.animalType ?
               this.homeAnimal.push({type: this.animalType, home: []}): console.error('The home already created');
        });
    };
};

AnimalHome.prototype.AnimalHomeStatistics = function () {
    this.homeAnimal.forEach((value => {
        if(value.type == this.animalType)
            console.log(`The ${value.type} home includes ${value.home.length} animals from allowed 10.`);
    }));
};

function Animal(animalType) {
    this.animalType = animalType;
};

Animal.prototype = new AnimalHome();

Animal.prototype.setAnimalHome = function () {
    this.homeAnimal.forEach((value) => {
        if(value.type == this.animalType)
            value.home.length < 10 ? value.home.push(value.type) : console.error('The home size is full');
    });
};

const dog = new Animal('dog');
const dogTwo = new Animal('dog');
const cat = new Animal('cat');
const catTwo = new Animal('cat');
dog.createAnimalHome();
cat.createAnimalHome();


dog.setAnimalHome();
cat.setAnimalHome();
dogTwo.setAnimalHome();
catTwo.setAnimalHome();

dog.AnimalHomeStatistics();
cat.AnimalHomeStatistics();

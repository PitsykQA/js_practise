'use strict';

const person = {
    isHuman: false,
    printIntroduction: function () {
        console.log(`My name is ${this.name}. Am I human? ${this.isHuman}`);
    }
};

const me = Object.create(person);

me.name = "Matthew"; // "name" is a property set on "me", but not on "person"
me.isHuman = true; // inherited properties can be overwritten

me.printIntroduction();

var test = Object.create(null);
var test2 = {};

console.log(test);

//Animal.prototype = Object.create(new SupperClass()) => Створить __proto__ в анімал в якому буде проперті Supper class,
// в наступному __proto__  буде всі методи з SupperClass які були додані в прототип

function AnimalHome(homeType) {
    this.homeType = homeType;
};

AnimalHome.prototype.homeAnimal = [];

AnimalHome.prototype.createAnimalHome = function (typeOfNome) {
    if (this.homeAnimal.length == 0) {
        this.homeAnimal.push({type: typeOfNome, home: []})
    } else {
        this.homeAnimal.forEach((value) => {
            if (value.type !== typeOfNome) {
                return this.homeAnimal.push({type: typeOfNome, home: []});
            } else {
                console.error('The home already created');
            };
        });
    };
};

AnimalHome.prototype.setAnimalHome = function () {

    this.homeAnimal.forEach((value) => {
        // need to add checking animal type;
        if(value.type == 'dog') {
            if (value.home.length < 5){
                value.home.push(value.type);
            } else { console.error('The home is full')};
        }
    })
};

// Link on fiddel with task https://jsfiddle.net/qf2bn7h1/12/

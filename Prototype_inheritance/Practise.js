'use strict';
//

function Human(firstName, lastName) {
    this.firstName = firstName,
        this.lastName = lastName,
        this.fullName = function() {
            return this.firstName + " " + this.lastName;
        }
}

Human.prototype.age = function(age) {
    this.age= age;
    return this.age;
}


let user = new Human("Virat", "Kohli");
let user2 = new Human("Arbat", "Kinhan");


console.log(user.__proto__ === user2.__proto__); // Both instances shared prototype of inherited object;
console.log(user.age(27));
console.log(user2.age(23));


function Person(){

}

Person.prototype.name = "Ashwin" ;
Person.prototype.age = 26;
Person.prototype.friends = ['Jadeja', 'Vijay'],//Arrays are of reference type in JavaScript
Person.prototype.sayName = function(){
    console.log(this.name);
};

let person1 = new Person();
let person2 = new Person();
person1.name = 'Andrii'; // Here person1 will create his own property name and will not go to the inherited object.
console.log(person1.name);
console.log(person2.name);

person1.friends.push('Andrii'); // Adding new friends will affect all instances of Person;
console.log(person2.friends);

//To solve the problems, we can define all the object-specific properties inside the constructor and all
// shared properties and methods inside the prototype as shown below:

function HumanTwo(name, age){
    this.name = name,
        this.age = age,
        this.friends = ["Jadeja", "Vijay"]
}
//Define the shared properties and methods using the prototype
HumanTwo.prototype.sayName = function(){
    console.log(this.name);
};

let person3 = new HumanTwo('Andrii', 27);
let person4 = new HumanTwo('Yulia',23);
person3.friends.push('Roman');
console.log(person3.friends); //[ 'Jadeja', 'Vijay', 'Roman' ]
console.log(person4.friends); //[ 'Jadeja', 'Vijay' ]


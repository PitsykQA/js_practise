'use strict';

let user = {};

user['name']='John';
user.age = 30;

user.sayHi = ()=>{
    console.log('Hello', user);
}

user.sayHi();


//Method shorthand

let newUser = {
    sayHi(){console.log('Hi new user')}
};
newUser.sayHi();


// this in methods

let showUser = {
    name: 'Andrii',
    age: 27 ,
    sayHi(){
        console.log(this.name, this.age);
    }
}
showUser.sayHi();

// BAD STYLE USE name of object instead of this
let showUserExample = {
    name: 'Andrii',
    age: 27 ,
    sayHi(){
        console.log(showUserExample.name, showUserExample.age);
    }
}

let badStyle = showUserExample;
showUserExample = null;
//badStyle.sayHi(); ERROR because showUserExample doesn't exist anymore. So this is much better to use.

// this with function

const badProgress = {
    progress: 10+'%'
};
const goodProgress = {
    progress: 90+'%'
};

function getProgress() {
    console.log(this.progress);
}

[badProgress.checkProgress, goodProgress.checkProgress] = [getProgress, getProgress];

badProgress.checkProgress();
goodProgress['checkProgress']();



    const obj = {
        name: "John",
        ref: this
    };

    console.log(obj.ref);

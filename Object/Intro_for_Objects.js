'use strict';

// There are 6 primitives: String, Number, Boolean, null, undefined, symbol;  They are primitive because they can include only one value;

// Square brackets assignment

let obj = {};
//add property
obj['Create property'] = true;
console.log(obj);
//delete property
delete obj['Create property'];
console.log(obj);



let objTwo = {};
const sum = "Andrii Test";

objTwo[sum] = true;
console.log(objTwo);


// Computed properties assigment
const name = 'Andrii';

let objThree = {
    [name]: true
};

console.log(objThree);


//Property value shorthand

function userInfo(name,age) {
    return {
        name, // The same as name: name
        age , // The same as age: age
        sex: "Men"
    }
}
console.log(userInfo('Andrii', 27));


// If property doesn't exist then returns undefined
let objFor = {
    name: true
};
console.log(objFor.name === undefined) // false because property exits;
console.log(objFor.time === undefined) // true because property doesn't exist;

// We can also check property using in;

console.log("name" in objFor);
console.log("test" in objFor);


// Walk over all object keys
const loop = {
    name: 'Andrii',
    year: 1991,
    position: 'AQA'
};

for(let item in loop){
    console.log(item); // show only properties
    console.log(loop[item]); // show properties value
}

//OBJECT DON'T RETURN VALUE IN THIS ORDER IN WHICH THEY WERE ASSIGNED.
// The phone codes go in the ascending sorted order, because they are integers. So we see 1, 41, 44, 49.
let codes = {
    "49": "Germany",
    "41": "Switzerland",
    "44": "Great Britain",
    // ..,
    "1": "USA"
};

for (let code in codes) {
    console.log(code); // 1, 41, 44, 49
}

// To fix this we need to change properties to string so we need to add  + for each of property

//Copying by reference
const country = {name:'Ukraine'};
const copyCountry = country;
console.log(country);
console.log(copyCountry);

//If we change name for one of object, for another one name will changed to. We don't copy the object. We copy the reference on the object;

copyCountry.name = 'Spain';
console.log(country); // Spain
console.log(copyCountry); // Spain

// CONST OBJECT
const changePropertyValue = {
    name: 'Ukraine'
};

changePropertyValue.name = 'Italy'; // No Error because we change property not object;
console.log(changePropertyValue);

//COPY PROPERTIS VARIABLE TO ANOTHER OBJECT
const copyVariables = {
    name:'blabla',
    year: 111,
    fullSize: true
}

const newCopiedObject = {};
for (let key in copyVariables){
    newCopiedObject[key] =  copyVariables[key];
}
console.log('First example', newCopiedObject);

const secondObject = {};
Object.assign(secondObject, copyVariables);
console.log('Second example', secondObject);

const thirdObject = Object.assign({}, secondObject);
console.log("thirdObject object",thirdObject)

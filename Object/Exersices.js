'use strict';
var obj = {
    name: 123,
};

function isObjectEmpty(object) {
    for (var key in obj) {
        if (key !== undefined) {
            return true;
            break;
        }
    }
    return false;
}

console.log(isObjectEmpty(obj));




let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
};


function getSum(obj) {
    var arr = [];
     for(const prop in obj){
        arr.push(obj[prop]);
     }
     const result = arr.reduce((acc,value) => {return acc + value});
     return result;
}

console.log(getSum(salaries));



const secondMethod = (obj)=>{
    let result = 0;
    for(const key in obj){
        let value = obj[key];
        result += value;
    }
    return result;
};

console.log(secondMethod(salaries));


function checkProperty() {
    if(salaries.John === undefined)
    console.log('Property is absent');
else
    console.log('Property is present');
    };


checkProperty();


// Make calls chaining
let ladder = {
    step: 0,
    up() {
        this.step++;
        return this;
    },
    down() {
        this.step--;
        return this;
    },
    showStep: function() { // shows the current step
        return this.step;
    }
};

console.log(ladder.up().down().up().up().up().up().showStep());



//CUNSTRUCTOR CALCULATOR

function Calculator(firstValue,secondValue) {
    this.firstValue = firstValue;
    this.secondValue = secondValue;

    this.sumValues = ()=> {
        return this.firstValue + this.secondValue;
    }

    this.mulValues = ()=> {
        return this.firstValue * this.secondValue;
    }
}

const result = new Calculator(10,20);
console.log('Sum values', result.sumValues());
console.log('Mul values', result.mulValues());


function Accumulator(setValue) {
    this.setValue = setValue;

    this.read = (value)=> {
        this.setValue += value;
    }
}

const showNumber = new Accumulator(1);

showNumber.read(5);
showNumber.read(15);
showNumber.read(15);

console.log(showNumber.setValue);

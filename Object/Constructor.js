'use strict';
//Usually, constructors do not have a return statement. Their task is to write all necessary stuff into this, and it automatically becomes the result.

function User(name) {
    console.log(this); // User = {}
    this.name = name;
    this.isAdmin = false;
    console.log(this); // User = {name: 'Andrii', isAdmin: false};
}

let user = new User('Andrii')

console.log(user.name, user.isAdmin);

//Constructor mode test: new.target. New set flat target flag to true
function Test() {
    console.log(new.target);
}
//Test(); // undefined
//new Test(); // function Test


// Methods in constructor

function showUser(name) {
    this.name = name;

    this.showName = ()=>{
        console.log(this.name);
    }
}

const andrii = new showUser('Andrii TEST');
andrii.showName();







// RETURN IN CONSTRUCTOR
// if constructor has a return statement, then the rule is simple:
//     1.If return is called with object, then it is returned instead of this.
//     2.If return is called with a primitive, it’s ignored.
// Usually constructors don’t have a return statement. Here we mention the special behavior with returning objects mainly for the sake of completeness.

// function BigUser() {
//     this.name = 'John';
//     return {name: 'GODZILLA'};
// }
//
// console.log(new BigUser().name);
//
//
// function BigUserTWO() {
//     this.name = 'John';
//     return 'Test'; // Test is ignored
// }
//
// console.log(new BigUserTWO().name);

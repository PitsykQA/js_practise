// There are two kinds of properties.

//The first kind is data properties. We already know how to work with them. All properties that we’ve been using till now were data properties.
// The second type of properties is something new. It’s accessor properties. They are essentially functions that work on getting and setting a value, but look like regular properties to an external code.

let obj = {
    get propertyName(){
        //getter, the code executed on getting obj.propName
    },
    set propertyName(value){
        // setter, the code executed on setting obj.propName = value
    }
};

let user = {
    name: 'Andrii',
    surname: 'Pitsyk',
    get fullName(){
        return `${this.name} ${this.surname}`;
    }
};

console.log(user.fullName); // From outside, an accessor property looks like a regular one. We don’t call user.fullName as a function, we read it normally: the getter runs behind the scenes.

// user.fullName = function () {
//     console.log('ERROR');
// };
// console.log(user.fullName()); => Will be error


let userTwo = {
    name: 'Andrii',
    surname: 'Pitsyk',
    get fullName(){
        return `${this.name} ${this.surname}`;
    },

    set fullName(value){
        [this.name, this.surname] = value.split(" ");
    }
};
userTwo.fullName = 'Alice Cooper'; // now we can set fullName what ever we want;
console.log(userTwo.name);
console.log(userTwo.surname);


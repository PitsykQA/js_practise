'use strict';

function sumArgs(...arg) {
   return arg.reduce((previousValue, currentValue) =>{
       return previousValue + currentValue;
   })
}

console.log(sumArgs(1,2,3));

function sumArgs() {
    const test = [...arguments];
    return test.reduce((previousValue, currentValue) =>{
       return previousValue + currentValue;
    });
}

console.log(sumArgs(1,2,4,6));


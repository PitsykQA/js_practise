'use strict';

function showFullName() {
    console.log(`${this.firstName} ${this.lastName}`);
}

const userObj = {
    firstName: 'Andrii',
    lastName: 'Pitsyk'
};

const userObjeTwo = {
    firstName: 'Oleh',
    lastName: 'Pitsyk'
};
// call pass different objects as "this"
showFullName.call(userObj);
showFullName.call(userObjeTwo);


// Use call arguments
function sayHi(phrase) {
    console.log(`${phrase} ${this.name}`);
}

const test = {
    name: 'Vova'
};

sayHi.call(test, 'hello');



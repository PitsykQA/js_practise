function sayHi(time, phrase) {
    console.log(`${time} ${this.name} ${phrase}`);
}

const user = {name: 'Andrii'};

const message =  ['10:00', 'Hello. Are you ready?'];
sayHi.apply(user, message);

// Call with spread has the same result;


'use strict';

const arr = [1,2,3,1,2,4,1,2,3];

const result = arr.filter((value, index, arr)=>{
    return index !== arr.indexOf(value) || index !== arr.lastIndexOf(value);
});

console.log(result);

// indexOf - start searching from the beginning of array
// lastIndexOf - start searching from the end of array

'use strict';
// E.x 1 . Unique arr
function unique(arr) {
    return [...new Set(arr)];
}

let values = ["Hare", "Krishna", "Hare", "Krishna", "Krishna", "Krishna", "Hare", "Hare", ":-O"];

console.log(unique(values));

// E.x 1 Filter anagrams

let map = new Map();

map.set("name", "John");
let keys = Array.from(map.keys());
keys.push('more');
console.log(keys);



'use strict'
let set = new Set();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

set.add(john);
set.add(pete);
set.add(mary);
set.add(john);
set.add(mary);

console.log(set.size); // 3. Repeated value is not stored again;

// Iteration

let fruits = new Set(["oranges", "apples", "bananas"]);
for (let value of fruits) console.log(value);

fruits.forEach(value => {console.log('For each', value)});

for (let value of fruits.keys()){
    console.log('Find by keys', value);
}

for (let value of fruits.values()){
    console.log('Find by values', value);
}

for (let value of fruits){
    console.log(fruits);
}

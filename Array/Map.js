'use strict';

let map = new Map();
map.set('1', 'str1');
map.set(1, 'num1');
map.set(true, 'bool1'); // a boolean key

// remember the regular Object  will convert keys to string
// Map keeps the type, so these two are different
console.log(map);
console.log(map.get(1)); // num1
console.log(map.get('1')); // str1
console.log(map.size);
console.log(map.has(1));// true
//console.log(map.delete());

// MAP USE OBJECT AS A KEY

let john ={name: 'John'};

let visitsCount = new Map();
visitsCount.set(john, 123);
console.log(visitsCount.get(john));

// MAP CHAIN CALLS
let value = 1;
let mapsChain = new Map();
mapsChain
    .set(1, value)
    .set(2, value)
    .set(3, value);
console.log(mapsChain);

// MAP FROM ARRAY TO OBJECT

let mapAsArray = new Map([
    [1, 'test'],
    [2, 'test2'],
    [3, 'test3']
]);

console.log(typeof mapAsArray); // object

// ITERATION OVER MAP

let recipeMap = new Map([
    ['cucumber', 500],
    ['tomatoes', 350],
    ['onion', 50]
]);

// iterate over keys (vegetables)
for(let vegetable of recipeMap.keys()){
    console.log(vegetable);
}

for(let amount of recipeMap.values()){
    console.log(amount);
}

for (let entry of recipeMap){
    console.log(entry);
}
let forEachItaration = new Map([
    ['name', 'Andrii'],
    ['year', 1991],
    ['position', 'Test Automation Engenier']
]);

forEachItaration.forEach((value, key) =>{
    console.log('KEY', key);
    console.log('VALUE', value);
});

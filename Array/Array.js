const fruits = ['apple'];
let copyFruits = fruits; // copy by reference (two variables reference the same array)

copyFruits.push('banana');
console.log(copyFruits);
console.log(fruits); // Also [ 'apple', 'banana' ].


// Here we show that copyFruits is the type object. But pls use copyFruits as copyFruits not as object.If you need obj with properties than
// push whole object;

const testArr = ['test','test2'];
console.log(testArr.length);
testArr.name = 'Andrii';
console.log(testArr);

// LOOPS

let arr = ["Apple", "Orange", "Pear"];

/* For loop using index */
for(let i = 0; i < arr.length; i++){
    console.log(arr[i]);
};

/* For of loop. We pass through values not their index */

for(const value of arr) {
    console.log('For of style', value);
};

/* For in loop. We also can use for in loop because array is an object. NOT RECOMMENDED BECAUSE IT IS SLOWER. */

for (const value in arr){  //never use
    console.log('For in style', value);
}



const homeWork = new Array('Jass', 'Blues');
homeWork.push('Rock-n-Roll');
const findMiddleValue = Math.round(homeWork.length/2);
console.log(findMiddleValue);

homeWork[findMiddleValue] = 'Classic';
console.log(homeWork);

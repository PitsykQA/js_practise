'use strict';

const nonUniqueElements = (data) => {
    const obj = {};
    const arr = [];
    data.forEach((value, index) => {
        if (obj[value]) {
            arr.push(value);
        }
        return obj.value;
    });
}

console.log(nonUniqueElements([1, 2, 3, 1, 3]));

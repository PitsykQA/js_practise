'use strict';
// E.X 1
function camelize(str) {
    const normData = str.split(/-/);
    const result = normData.map((value, index) =>{
       return index == 0 ? value : value.charAt(0).toUpperCase() + value.slice(1);
    });
    return result.join('');
}
console.log(camelize("list-style-image"));

// E.X 2
const exTwo = [5, 3, 8, 1];

const filterRange = (arr, a, b) => {
    const filteredValue = arr.filter(value => {
       return value == a || value == b ? value : null;
    })
    return filteredValue;
};

console.log(filterRange(exTwo,  1, 4));
console.log(exTwo);


// E.X 3

let exThree = [5, 2, 1, -10, 8];

// var exThreeRes = exThree.reduce((acc, value) =>{
// })
const exThreeRes = exThree.sort((a, b) => {
    return a - b;
});

console.log(exThreeRes);

// E.X Map to names

let john = { name: "John", age: 25 };
let pete = { name: "Pete", age: 30 };
let mary = { name: "Mary", age: 28 };

let users = [ john, pete, mary ];

let names = users.map(value => {
   return value.name;
});
console.log(...names);


//E.X Map to objects
let mikle = { name: "Mikle", surname: "Jordan", id: 1 };
let irvin = { name: "Irvin", surname: "Hunt", id: 2 };
let koby = { name: "Koby", surname: "Brient", id: 3 };

let usersRes = [ mikle, irvin, koby ];

const resultOfUsers = usersRes.map((value) => {
   const test = {};
   test['fullName'] = `${value.name}  ${value.surname}`;
   test['id'] = value.id;
   return test;
});
console.log(resultOfUsers);


//E.X Sort users by age

let roben = { name: "Roben", age: 25 };
let feder = { name: "Feder", age: 30 };
let nadal = { name: "Nadal", age: 28 };

let sortUserByAge = [ roben, feder, nadal ];

const sortByAge = (arr) =>{
    return arr.sort((a,b) =>{
        a.age > b.age ?  1 : -1;
    });
};

console.log(sortByAge(sortUserByAge));

// E.X => Shuffle an array
let shuffleArr = [1, 2, 3];

function shuffle(arr) {
  return arr.sort(() =>  Math.random() - 0.5);
};

console.log(shuffle(shuffleArr));

// E.X => "FILTER UNIQUE ARRAY MEMBERS";
let strings = ["Hare", "Krishna", "Hare", "Krishna",
    "Krishna", "Krishna", "Hare", "Hare", ":-O"];

const stringFirstResult = new Set(strings);
console.log(stringFirstResult);

const obj = {};
const stringSecondResult = strings.filter((value => {
     if(obj[value] === undefined){
        return obj[value]= '1';
     }
}));

console.log(stringSecondResult);

// form object and search by object;
function unique(arr) {
    let result = [];

    for (let str of arr) {
        if (!result.includes(str)) {
            result.push(str);
        }
    }
    return result;
}

console.log(unique(strings));

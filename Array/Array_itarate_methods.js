// For each

const arr = ['I','will be', 'automation', 'tester'];
arr.forEach((value, index, array) =>{
 //   console.log('Value', value);
});


// Search in arr
const searchFirst = ['I','will be', 'automation', 'tester'];
console.log(searchFirst.indexOf('tester')); // return possitions or -1 if value is not in arr
console.log(searchFirst.lastIndexOf('tester')); // Search from the end of arr.
console.log(searchFirst.includes('tester')); // return true or false


// FILTER
let users = [
    {id: 1, name: "John"},
    {id: 2, name: "Pete"},
    {id: 3, name: "Mary"}
];

let result  = users.filter(item => item.id <3);
console.log(result);

// TRANSFORMING OR REORDERING THE ARRAY

/* MAP METHOD*/
const mapExample = [1,3,12,33,123,4,5,2234,10];

const mapResult = mapExample.map((value) => {
  if (value <= 10){
      return value;
  }
});

console.log(mapResult);


/* REVERSE METHOD*/

let reverseExample = [1,2,3,4,5];
reverseExample.reverse();
console.log(reverseExample);

/*SPLIT METHID */
let splitExample = 'Andrii, Test, Igor, Roman';
console.log(splitExample.split(', ')); //[ 'Andrii', 'Test', 'Igor', 'Roman' ]

/*JOIN */
let joinExample = ['Andrii', 'Test', 'Igor', 'Roman'];
console.log(joinExample.join(';'));  // Andrii;Test;Igor;Roman





/*REDUCE & REDUCE RIGHT*/
let value = arr.reduce(function(previousValue, item, index, array) {
    // previousValue  is the result of the previous function call, initial for the first call
}, 'initial');

let reduceExample = [1,2,3,4,5];
                                        //sum = 0(initial value); current = 1;
let resultOfReduce = reduceExample.reduce((sum, current ) => sum + current, 0);
console.log(resultOfReduce);

//As we can see, the result of the previous call becomes the first argument of the next one.

let reduceExampleSecond = [1,2,3,4,5];
                                                    //sum = 1(1st elem from array); current = 2;
let reduceExampleSecondRestul = reduceExampleSecond.reduce((sum, current ) => sum + current);
console.log(reduceExampleSecondRestul);
// The result is the same. That’s because if there’s no initial, then reduce takes the first element of the array as the initial value and starts the iteration from the 2nd element.

// The method arr.reduceRight does the same, but goes from right to left.


/* Array.isArray */
const checkIsArray = [1,2,3];
console.log(Array.isArray(checkIsArray)); // true
console.log(Array.isArray({})); // false


/*SORT METHOD */

let exOfSort = [5, 2, 1, -10, 8];
const exOfSortRes = exOfSort.sort((a, b) => {
    return a > b ?  1 :  -1;
    // This is the logic of sort method;
});

console.log(exOfSortRes);

'use strict';
//In the previous chapter we saw methods map.keys(), map.values(), map.entries(). For set also method valid.
// Plain objects also support similar methods, but the syntax is a bit different.

const plainObj = {name: 'andrii', year: 1991};
console.log(Object.keys(plainObj)); // return array of obj property
console.log(Object.values(plainObj)); // return array of obj values

let user = {
    name: 'John',
    age: 30
};
for (let value of Object.values(user)){
    console.log(value);
}

// E.X 1

let salaries = {
  'John': 100,
  'Pete': 300,
  'Marry': 250
};

function sumSalaries(salaries) {
    let result = 0;
    for (let value of Object.values(salaries)) {
        result += value;
    }
    return result;
    // or
    //  return Object.values(salaries).reduce((a, b) => a + b, 0) // 650
}
console.log(sumSalaries(salaries));

function sizeOfObject(obj) {
    return Object.keys(obj).length; // Object.keys return and array and we get length;
}

console.log(sizeOfObject(salaries));

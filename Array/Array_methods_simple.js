// Delete elem from arr

const first = ['I', 'love', 'JS'];

delete first[1]; // leave empty value because we delete a key not a value. This is good for objects but not for arr
console.log(first);


// SPLICE Method

const second = ['I', 'love', 'JS'];
second.splice(1,1);
//First 1 -> set from which position we should remove. 2-nd show how many elements should be deleted.
console.log(second);

const secondNext = ['I', 'love', 'JS'];

secondNext.splice(0,3, 'I','will be', 'automation', 'tester'); // Delete 3 elements and insert new one;
console.log(secondNext);


const secondNextOne = ['I', 'love', 'JS'];
secondNextOne.splice(3,0, '.', 'I will be AQA'); // Don't delete anything. Only add the text at the end of the arr;
console.log(secondNextOne);


// Negative index allowed;
let negativeIndex = [1,2,4];

negativeIndex.splice(-1,0,3,5);// start from the end;
console.log(negativeIndex);


// SLice
const sliceFirst = ['I','will be', 'automation', 'tester'];

const result = sliceFirst.slice(0, 2); // This method doesn't modify arr. He returns only result.
console.log('SLICE', sliceFirst); // Result will be the same as assigment above;
console.log(result);// Here will be result of the values;


// CONCAT -> The method doesn't change the origin array. He returns only result;
const concatExample = [1,2,3];
console.log(concatExample.concat([4,5]));
console.log(concatExample.concat([3, 4], [5, 6]));
console.log(concatExample.concat([3, 4, 5],6,7));

let userInfo ={
    name: 'Andrii',
    year: 1991
};

console.log(concatExample.concat(userInfo)); // Object will be assigned
